export const queryParams = params => {
  let query = '';

  try {
    query = Object.keys(params)
      .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
      .join('&');
  } catch (err) {
    console.error('Error parsing object to url encoded string', params);
    console.error(err);
  }

  return query;
};
