import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PentahoStore from './PentahoStore';
import containerStyles from './PentahoArtifact.style';

class PentahoPlaceHolder extends Component {
  componentDidMount() {
    this.requirejsHandler();
  }

  callDashRender() {
    const Dashboard = PentahoStore.get(this.props.id);
    const Instance = new Dashboard('pentahoArtifactPlaceholder');

    Instance.render();
  }

  requirejsHandler() {
    const { id, type, pentahoUrl, url } = this.props;
    const isRequire = type === 'requirejs';
    const hasDash = PentahoStore.has(id);

    if (isRequire && !hasDash) {
      const module = `${pentahoUrl}plugin/pentaho-cdf-dd/api/renderer/getDashboard?path=${url}`;

      window.require(
        [module],
        Dash => {
          PentahoStore.add(id, Dash);
          this.callDashRender();
        },
        err => {
          console.error('Error loading module:');
          console.error(err.requireModules);
          console.error(err);
        }
      );
    } else if (isRequire && hasDash) {
      if (document.getElementById('pentahoArtifactPlaceholder')) {
        this.callDashRender();
      }
    }
  }

  renderIframe() {
    const { pentahoUrl, url } = this.props;
    const pentahoUri = pentahoUrl + url;

    return (
      <iframe
        title="Pentaho Embedded Artifact"
        src={pentahoUri}
        style={containerStyles}
      />
    );
  }

  renderRequirejs() {
    return <div id="pentahoArtifactPlaceholder" />;
  }

  render() {
    return this.props.type === 'iframe'
      ? this.renderIframe()
      : this.renderRequirejs();
  }
}

PentahoPlaceHolder.propTypes = {
  id: PropTypes.string,
  type: PropTypes.oneOf(['iframe', 'requirejs']).isRequired,
  pentahoUrl: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
};

export default PentahoPlaceHolder;
